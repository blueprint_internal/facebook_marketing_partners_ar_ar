
function MapTwo(resources)
{
	MapTwo.resources = resources;
}
MapTwo.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 1000, Phaser.CANVAS, 'MapTwo', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},


	preload: function()
	{

        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 1000;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', MapTwo.resources.bckg);
		this.game.load.image('redcirc', MapTwo.resources.redcircle);
		this.game.load.spritesheet('industry', MapTwo.resources.pagebtnsprite, 50, 50);
		this.game.load.spritesheet('icondot', MapTwo.resources.icondot, 10, 10);
		this.game.load.image('icondotred', MapTwo.resources.icondotred);
		this.game.load.spritesheet('icon000', MapTwo.resources.icon000, 110, 110);
		this.game.load.spritesheet('icon001', MapTwo.resources.icon001, 110, 110);
		this.game.load.spritesheet('icon002', MapTwo.resources.icon002, 110, 110);
		this.game.load.spritesheet('icon003', MapTwo.resources.icon003, 110, 110);
		this.game.load.spritesheet('icon004', MapTwo.resources.icon004, 110, 110);
		this.game.load.spritesheet('icon005', MapTwo.resources.icon005, 110, 110);
		this.game.load.spritesheet('icon006', MapTwo.resources.icon006, 110, 110);
		this.game.load.spritesheet('icon007', MapTwo.resources.icon007, 110, 110);
		this.game.load.spritesheet('icon008', MapTwo.resources.icon008, 110, 110);
		this.game.load.spritesheet('icon009', MapTwo.resources.icon009, 110, 110);
		this.game.load.image('slab', MapTwo.resources.slab);

	},

	create: function(evt)
	{
     
    //Call the animation build function
    this.parent.buildAnimation();



	},

	cycleData: function(evt){

	},


	textOff: function (evt){

		//console.log("evt" + evt);

	    for (var i=0;i<10;i++){ 

        var nowBut = this.butArray[i];

        nowBut.alpha = 0; 

       //var nowParen = nowBut.parent;
		 //for (prop in nowParen){
          // console.log("prop : " + prop +" : "+ nowParen[prop]);
		//}
		//.textId.alpha = 0; 

		}
	}
	,

	actionOnClickCountry: function(evt){


		//this.pfield_2 =


		this.pfield_2.setText(this.cords[evt.btnId].btnText);


		this.icondotred.x = evt.x;
	    this.icondotred.y = evt.y;

	

	},

	up: function(evt)
			{

	}
		,
	over: function(evt)
			{

	}
		,
	out: function(evt)
			{

	}
	,

	buildAnimation: function()
	{

	this.nowBtn = "";

     //Background
     this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.bckg.anchor.set(0.5);

     this.slab = this.game.add.sprite(this.game.world.centerX, 55, 'slab');
     this.slab.anchor.set(0.5);

     this.countDown = 100; 


 

    //STYLE
    


     var headerDisplayStyle = {font:"bold 35px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 600, align: "Center",lineSpacing: -10 };

    var listDisplayStyle = {font:"bold 20px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 700, align: "left",lineSpacing: -10 };

    var styleIndustryIconText = {font:"bold 12px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 110, align: "Center",lineSpacing: -1 };


        var styleFunnelTemp = {font:"bold 12px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 600, align: "Center",lineSpacing: 0 };

        //


//Textfield that displays the name of the lower icon.
		this.pfield_1 = this.game.add.text(this.game.world.centerX, 55, MapTwo.resources.ico1text, headerDisplayStyle);
		this.pfield_1.anchor.set(0.5);

//Text field that displays the country name.
		this.pfield_2 = this.game.add.text(50, 550, "", listDisplayStyle);
		//this.pfield_2.anchor.set(0.5);

//Icon dots that mark the location of countries.
	this.butIconArray = ['icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot'];


//Icons that appear at the bottom of the page
	this.industryIconArray = ['icon000','icon001','icon002','icon003','icon004','icon005','icon006','icon007','icon008','icon009','icon010','icon011','icon012','icon013','icon014','icon015','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000'];

//An array of coordinates keyed to country names 
this.cords = [{btnText:'Argentina' , xLoc:188.99999809265137 , yLoc:483.85714503696977 } , {btnText:'Australia' , xLoc:672.9999980926514 , yLoc:437.8571425846644 } , {btnText:'Austria' , xLoc:379.99999809265137 , yLoc:203.85714067731578 } , {btnText:'Belgium' , xLoc:362.8691983122363 , yLoc:199.1561181434599 } , {btnText:'Brazil' , xLoc:230.99999809265137 , yLoc:388.85714503696977 } , {btnText:'Canada' , xLoc:99.68847114349079 , yLoc:153.6270582999554 } , {btnText:'Chile' , xLoc:181.99999809265137 , yLoc:446.85714503696977 } , {btnText:'China' , xLoc:596.9999980926514 , yLoc:255.8571428571429 } , {btnText:'Colombia' , xLoc:178.99999809265137 , yLoc:350.85714667184016 } , {btnText:'Czech Republic' , xLoc:393.99999809265137 , yLoc:208.85714067731578 } , {btnText:'Denmark' , xLoc:371.5976331360947 , yLoc:174.43786440516354 } , {btnText:'Finland' , xLoc:414.99999809265137 , yLoc:143.85714067731578 } , {btnText:'France' , xLoc:355.99999809265137 , yLoc:217.85714394705633 } , {btnText:'Germany' , xLoc:380.99999809265137 , yLoc:188.85714667184016 } , {btnText:'Hong Kong' , xLoc:606.9999980926514 , yLoc:297.8571428571429 } , {btnText:'India' , xLoc:538.9999980926514 , yLoc:299.85714503696977 } , {btnText:'Indonesia' , xLoc:642.9999980926514 , yLoc:386.8571428571429 } , {btnText:'Ireland' , xLoc:321.99999809265137 , yLoc:201.85714503696977 } , {btnText:'Israel' , xLoc:436.99999809265137 , yLoc:266.85714667184016 } , {btnText:'Italy' , xLoc:386.99999809265137 , yLoc:236.85714503696977 } , {btnText:'Japan' , xLoc:688.9999980926514 , yLoc:239.85714503696977 } , {btnText:'Korea' , xLoc:655.9999980926514 , yLoc:253.85714503696977 } , {btnText:'Malaysia' , xLoc:613.9999980926514 , yLoc:352.8571428571429 } , {btnText:'Mexico' , xLoc:104.67289481950326 , yLoc:275.7454315715225 } , {btnText:'Netherlands' , xLoc:358.99999809265137 , yLoc:187.85714667184016 } , {btnText:'New Zealand' , xLoc:750.9999980926514 , yLoc:492.85714667184016 } , {btnText:'Nigeria' , xLoc:370.99999809265137 , yLoc:331.85714272090377 } , {btnText:'Norway' , xLoc:369.99999809265137 , yLoc:141.85714306150157 } , {btnText:'Panama' , xLoc:162.99999809265137 , yLoc:330.85714667184016 } , {btnText:'Paraguay' , xLoc:217.99999809265137 , yLoc:424.85714503696977 } , {btnText:'Peru' , xLoc:177.99999809265137 , yLoc:389.8571430615016 } , {btnText:'Philippines' , xLoc:653.9999980926514 , yLoc:337.8571428571429 } , {btnText:'Poland' , xLoc:402.99999809265137 , yLoc:193.85714667184016 } , {btnText:'Romania' , xLoc:412.99999809265137 , yLoc:219.85714667184016 } , {btnText:'Russia' , xLoc:591.9999980926514 , yLoc:148.85714667184016 } , {btnText:'Saudi Arabia' , xLoc:453.99999809265137 , yLoc:300.85714667184016 } , {btnText:'Singapore' , xLoc:598.9999980926514 , yLoc:352.8571406773158 } , {btnText:'South Africa' , xLoc:409.96884497526645 , yLoc:411.57098421267403 } , {btnText:'Spain' , xLoc:341.99999809265137 , yLoc:246.85714394705633 } , {btnText:'Sweden' , xLoc:393.1232091690544 , yLoc:123.55301034484698 } , {btnText:'Switzerland' , xLoc:371.74721189591077 , yLoc:219.18215670107014} , {btnText:'Turkey' , xLoc:436.99999809265137 , yLoc:247.85714503696977 } , {btnText:'United Arab Emirates' , xLoc:486.99999809265137 , yLoc:291.85714667184016 } , {btnText:'United Kingdom' , xLoc:336.99999809265137 , yLoc:172.85714503696977 } , {btnText:'United States Of America' , xLoc:111.17478510028653 , yLoc:241.60458365279143 } , {btnText:'Vietnam' , xLoc:595.9999980926514 , yLoc:326.8571428571429 } ];



////////////////////




this.adtech =[  " Argentina", " Australia", " Austria", " Belgium", " Brazil", " Canada", " Chile", " China", " Colombia", " Czech Republic", " Denmark", " Finland", " France", " Germany", " India", " Indonesia", " Ireland", " Israel", " Italy", " Japan", " Malaysia", " Mexico", " Netherlands", " New Zealand", " Nigeria", " Norway", " Panama", " Paraguay", " Peru", " Phillippines", " Poland", " Portugal", " Romania", " Russia", " Saudi Arabia", " Singapore", " South Africa", " Korea", " Spain", " Sweden", " Switzerland", " Taiwan", " Thailand", " Turkey", " United Arab Emirates", " United Kingdom", " United States of America", " Vietnam "  ];

this.contentmarketing = [ " Argentina", " Australia", " Austria", " Belgium", " Bulgaria", " Canada", " Chile", " Colombia", " Costa Rica", " Czech Republic", " Denmark", " Egypt", " Finland", " France", " Germany", " Hong Kong", " Ireland", " Israel", " Italy", " Japan", " Macedonia", " Mexico", " Netherlands", " New Zealand", " Norway", " Panama", " Paraguay", " Peru", " Poland", " Portugal", " Romania", " Russia", " Saudi Arabia", " Singapore", " South Africa", " Spain", " Sweden", " Switzerland", " Turkey", " United Arab Emirates", " United Kingdom", " United States of America", " Uruguay", " Venezuela", " Vietnam "  ];

this.communitymanagement=[ " Argentina", " Australia", " Austria", " Belgium", " Brazil", " Bulgaria", " Canada", " Chile", " Colombia", " Costa Rica", " Czech Republic", " Denmark", " Egypt", " Finland", " France", " Germany", " Hong Kong", " India", " Indonesia", " Ireland", " Israel", " Italy", " Japan", " Macedonia", " Malaysia", " Mexico", " Netherlands", " New Zealand", " Nigeria", " Norway", " Panama", " Paraguay", " Peru", " Philippines", " Poland", " Portugal", " Romania", " Russia", " Saudi Arabia", " Singapore", " Slovakia", " South Africa", " Korea", " Spain", " Sweden", " Switzerland", " Thailand", " Turkey", " United Arab Emirates", " United Kingdom", " United States of America", " Uruguay", " Venezuela"  ]; 

this.audienceonboarding =[  " Australia", " Austria", " Canada", " Czech Republic", " Denmark", " Finland", " France", " Germany", " Ireland", " Israel", " Italy", " Japan", " Mexico", " Netherlands", " New Zealand", " Norway", " Panama", " Paraguay", " Peru", " Poland", " Portugal", " Romania", " Saudi Arabia", " South Africa", " Sweden", " Switzerland", " United Arab Emirates", " United Kingdom", " United States of America "];

this.audiencedataproviders=[  " Australia", " France", " Germany", " United Kingdom", " United States of America "  ];

this.fbx =[  " Argentina", " Australia", " Austria", " Belgium", " Brazil", " Canada", " Chile", " China", " Colombia", " Costa Rica", " Denmark", " Egypt", " Finland", " France", " Germany", " Hong Kong", " India", " Indonesia", " Ireland", " Israel", " Italy", " Japan", " Malaysia", " Mexico", " Netherlands", " New Zealand", " Norway", " Peru", " Philippines", " Poland", " Russia", " Saudi Arabia", " Singapore", " Korea", " Spain", " Sweden", " Switzerland", " Taiwan", " Thailand", " United Kingdom", " United States of America", " Venezuela", " Vietnam "  ];

this.mediabuying = [  " United States of America "  ];

this.measurement = [  " Brazil", " China", " Germany", " Hong Kong", " Israel", " Japan", " Korea", " Taiwan", " Thailand", " United Kingdom", " United States of America "  ]; 

this.smallbusinesssolutions = [  " Argentina", " Australia", " Brazil", " Canada", " Ireland", " Japan", " United Kingdom", " United States of America " ];

this.indusArray = [this.fbx,this.adtech,this.measurement,this.audienceonboarding,this.audiencedataproviders,this.mediabuying,this.communitymanagement,this.smallbusinesssolutions, this.contentmarketing];


///////////////////



	this.countries = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Chile" , " China" , " Colombia" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Nigeria" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

	

		this.testy = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark " ];


		this.butArray = new Array();
		this.countryDotArray = new Array();
		this.localTextArray = MapTwo.resources.iconArray;


		this.xLoop = 0;
		this.yLoop = 0;

		this.xLoopPlus = 0;

		this.xLoopTest = 0;


        for (var i=0;i<9;i++){ 

			if (this.xLoop < 3){
				this.xLoop += 1; 
				
			}else{

				this.yLoop += 1;
				this.xLoop = 1 ;

			}

            //This LAYS OUT THE BUTTONS ALONG THE BOTTOM
	  		var temp = this.game.add.button( (this.xLoop * 180), 180 + (this.yLoop * 100), this.industryIconArray[i], this.actionOnClick, this, 1, 0, 2);

	  		if (this.yLoop == 1 || this.yLoop == 3 ){

				 temp.x += 90;

			}
			
			temp.btnId = i;

			temp.scale.x = .8;
			temp.scale.y = .8;

            ///This is the array data coming from the JSON
			temp.textData = this.localTextArray[i];
   
		    temp.anchor.set(0.5);
			//temp.alpha = 0;

			var speedAlpha = 100 + i*100;
			//var tween1 = this.game.add.tween(temp).to({alpha: 1}, 100, Phaser.Easing.Exponential.Out, true, speedAlpha);
			this.butArray.push(temp);

		};


	this.redcirc = this.game.add.sprite(this.game.world.centerX - 1000, this.game.world.centerY-1000, 'redcirc')

	this.redcirc.anchor.set(0.5);

	},

	butOff: function (evt){


	},

	actionOnClick: function(evt){

    this.redcirc.x = evt.x;
	this.redcirc.y = evt.y;

	this.redcirc.scale.x = .50;
	this.redcirc.scale.y = .50;

    this.pfield_2.setText("");

	this.testy2 = new Array();

    this.testy2 = this.indusArray[evt.btnId];

	this.localArray = MapTwo.resources.iconArray;
	this.pfield_1.setText(this.localArray[evt.btnId]);

	this.pfield_2.setText(this.testy2);

	},

	render: function()
	{
	
	}

}