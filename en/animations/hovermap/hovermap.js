
function Hovermap(resources)
{
	Hovermap.resources = resources;
}
Hovermap.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 1000, Phaser.CANVAS, 'Hovermap', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},


	preload: function()
	{

        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 1000;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Hovermap.resources.bckg);
		this.game.load.image('redcirc', Hovermap.resources.redcircle);
		this.game.load.spritesheet('industry', Hovermap.resources.pagebtnsprite, 50, 50);
		this.game.load.spritesheet('icondot', Hovermap.resources.icondot, 10, 10);
		this.game.load.image('icondotred', Hovermap.resources.icondotred);
		this.game.load.spritesheet('icon000', Hovermap.resources.icon000, 110, 110);
		this.game.load.spritesheet('icon001', Hovermap.resources.icon001, 110, 110);
		this.game.load.spritesheet('icon002', Hovermap.resources.icon002, 110, 110);
		this.game.load.spritesheet('icon003', Hovermap.resources.icon003, 110, 110);
		this.game.load.spritesheet('icon004', Hovermap.resources.icon004, 110, 110);
		this.game.load.spritesheet('icon005', Hovermap.resources.icon005, 110, 110);
		this.game.load.spritesheet('icon006', Hovermap.resources.icon006, 110, 110);
		this.game.load.spritesheet('icon007', Hovermap.resources.icon007, 110, 110);
		this.game.load.spritesheet('icon008', Hovermap.resources.icon008, 110, 110);
		this.game.load.spritesheet('icon009', Hovermap.resources.icon009, 110, 110);
		this.game.load.spritesheet('icon010', Hovermap.resources.icon010, 110, 110);
		this.game.load.spritesheet('icon011', Hovermap.resources.icon011, 110, 110);
		this.game.load.spritesheet('icon012', Hovermap.resources.icon012, 110, 110);
		this.game.load.spritesheet('icon013', Hovermap.resources.icon013, 110, 110);
		this.game.load.spritesheet('icon014', Hovermap.resources.icon014, 110, 110);
		this.game.load.spritesheet('icon015', Hovermap.resources.icon015, 110, 110);
		this.game.load.image('slab', Hovermap.resources.slab);

	},

	create: function(evt)
	{
     
    //Call the animation build function
    this.parent.buildAnimation();



	},

	cycleData: function(evt){

	},


	textOff: function (evt){

		//console.log("evt" + evt);

	    for (var i=0;i<10;i++){ 

        var nowBut = this.butArray[i];

        nowBut.alpha = 0; 

       //var nowParen = nowBut.parent;
		 //for (prop in nowParen){
          // console.log("prop : " + prop +" : "+ nowParen[prop]);
		//}
		//.textId.alpha = 0; 

		}
	}
	,

	actionOnClickCountry: function(evt){


		//this.pfield_2 =


		this.pfield_2.setText(this.cords[evt.btnId].btnText);


		this.icondotred.x = evt.x;
	    this.icondotred.y = evt.y;

	

	},

	up: function(evt)
			{

	}
		,
	over: function(evt)
			{

	}
		,
	out: function(evt)
			{

	}
	,

	buildAnimation: function()
	{

	this.nowBtn = "";

     //Background
     this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.bckg.anchor.set(0.5);

     this.slab = this.game.add.sprite(this.game.world.centerX, 55, 'slab');
     this.slab.anchor.set(0.5);

     this.countDown = 100; 


 

    //STYLE
    

    var headerDisplayStyle = {font:"bold 35px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 600, align: "Center",lineSpacing: -10 };

    var listDisplayStyle = {font:"bold 20px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 700, align: "left",lineSpacing: -10 };

    var styleIndustryIconText = {font:"bold 12px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 110, align: "Center",lineSpacing: -1 };


        var styleFunnelTemp = {font:"bold 12px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 600, align: "Center",lineSpacing: 0 };

        //


//Textfield that displays the name of the lower icon.
		this.pfield_1 = this.game.add.text(this.game.world.centerX, 55, Hovermap.resources.ico1text, headerDisplayStyle);
		this.pfield_1.anchor.set(0.5);

//Text field that displays the country name.
		this.pfield_2 = this.game.add.text(50, 550, "", listDisplayStyle);
		//this.pfield_2.anchor.set(0.5);

//Icon dots that mark the location of countries.
	this.butIconArray = ['icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot','icondot'];


//Icons that appear at the bottom of the page
	this.industryIconArray = ['icon000','icon001','icon002','icon003','icon004','icon005','icon006','icon007','icon008','icon009','icon010','icon011','icon012','icon013','icon014','icon015','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000','icon000'];

//An array of coordinates keyed to country names 
this.cords = [{btnText:'Argentina' , xLoc:188.99999809265137 , yLoc:483.85714503696977 } , {btnText:'Australia' , xLoc:672.9999980926514 , yLoc:437.8571425846644 } , {btnText:'Austria' , xLoc:379.99999809265137 , yLoc:203.85714067731578 } , {btnText:'Belgium' , xLoc:362.8691983122363 , yLoc:199.1561181434599 } , {btnText:'Brazil' , xLoc:230.99999809265137 , yLoc:388.85714503696977 } , {btnText:'Canada' , xLoc:99.68847114349079 , yLoc:153.6270582999554 } , {btnText:'Chile' , xLoc:181.99999809265137 , yLoc:446.85714503696977 } , {btnText:'China' , xLoc:596.9999980926514 , yLoc:255.8571428571429 } , {btnText:'Colombia' , xLoc:178.99999809265137 , yLoc:350.85714667184016 } , {btnText:'Czech Republic' , xLoc:393.99999809265137 , yLoc:208.85714067731578 } , {btnText:'Denmark' , xLoc:371.5976331360947 , yLoc:174.43786440516354 } , {btnText:'Finland' , xLoc:414.99999809265137 , yLoc:143.85714067731578 } , {btnText:'France' , xLoc:355.99999809265137 , yLoc:217.85714394705633 } , {btnText:'Germany' , xLoc:380.99999809265137 , yLoc:188.85714667184016 } , {btnText:'Hong Kong' , xLoc:606.9999980926514 , yLoc:297.8571428571429 } , {btnText:'India' , xLoc:538.9999980926514 , yLoc:299.85714503696977 } , {btnText:'Indonesia' , xLoc:642.9999980926514 , yLoc:386.8571428571429 } , {btnText:'Ireland' , xLoc:321.99999809265137 , yLoc:201.85714503696977 } , {btnText:'Israel' , xLoc:436.99999809265137 , yLoc:266.85714667184016 } , {btnText:'Italy' , xLoc:386.99999809265137 , yLoc:236.85714503696977 } , {btnText:'Japan' , xLoc:688.9999980926514 , yLoc:239.85714503696977 } , {btnText:'Korea' , xLoc:655.9999980926514 , yLoc:253.85714503696977 } , {btnText:'Malaysia' , xLoc:613.9999980926514 , yLoc:352.8571428571429 } , {btnText:'Mexico' , xLoc:104.67289481950326 , yLoc:275.7454315715225 } , {btnText:'Netherlands' , xLoc:358.99999809265137 , yLoc:187.85714667184016 } , {btnText:'New Zealand' , xLoc:750.9999980926514 , yLoc:492.85714667184016 } , {btnText:'Nigeria' , xLoc:370.99999809265137 , yLoc:331.85714272090377 } , {btnText:'Norway' , xLoc:369.99999809265137 , yLoc:141.85714306150157 } , {btnText:'Panama' , xLoc:162.99999809265137 , yLoc:330.85714667184016 } , {btnText:'Paraguay' , xLoc:217.99999809265137 , yLoc:424.85714503696977 } , {btnText:'Peru' , xLoc:177.99999809265137 , yLoc:389.8571430615016 } , {btnText:'Philippines' , xLoc:653.9999980926514 , yLoc:337.8571428571429 } , {btnText:'Poland' , xLoc:402.99999809265137 , yLoc:193.85714667184016 } , {btnText:'Romania' , xLoc:412.99999809265137 , yLoc:219.85714667184016 } , {btnText:'Russia' , xLoc:591.9999980926514 , yLoc:148.85714667184016 } , {btnText:'Saudi Arabia' , xLoc:453.99999809265137 , yLoc:300.85714667184016 } , {btnText:'Singapore' , xLoc:598.9999980926514 , yLoc:352.8571406773158 } , {btnText:'South Africa' , xLoc:409.96884497526645 , yLoc:411.57098421267403 } , {btnText:'Spain' , xLoc:341.99999809265137 , yLoc:246.85714394705633 } , {btnText:'Sweden' , xLoc:393.1232091690544 , yLoc:123.55301034484698 } , {btnText:'Switzerland' , xLoc:371.74721189591077 , yLoc:219.18215670107014} , {btnText:'Turkey' , xLoc:436.99999809265137 , yLoc:247.85714503696977 } , {btnText:'United Arab Emirates' , xLoc:486.99999809265137 , yLoc:291.85714667184016 } , {btnText:'United Kingdom' , xLoc:336.99999809265137 , yLoc:172.85714503696977 } , {btnText:'United States Of America' , xLoc:111.17478510028653 , yLoc:241.60458365279143 } , {btnText:'Vietnam' , xLoc:595.9999980926514 , yLoc:326.8571428571429 } ];



	this.countries = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Chile" , " China" , " Colombia" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Nigeria" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.automotive = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States" , " Vietnam " ];

		this.cpg = [ " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Chile" , " Colombia" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Germany" , " Hong Kong" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Nigeria" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.ecommerce = [ " Argentina" , " Australia" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Canada" , " Chile" , " Colombia" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.energyutil = [ " Not Available" ];

		this.education = [ " Argentina" , " Australia" , " Brazil" , " Canada" , " France" , " Germany" , " India" , " Indonesia" , " Ireland" , " Italy" , " Japan" , " Malaysia" , " New Zealand" , " Philippines" , " Russia" , " Singapore" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.entertainment = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Chile" , " Colombia" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.financialservice = [ " Argentina" , " Australia" , " Austria" , " Brazil" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.gaming =[ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " China" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " India" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.govtpoli = [ " Not Available" ];

		this.orgassociations = [ " Australia" , " Austria" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Ireland" , " Israel" , " Italy" , " Japan" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Poland" , " Romania" , " Saudi Arabia" , " South Africa" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America " ];

		this.proservices =[ " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " India" , " Ireland" , " Israel" , " Italy" , " Japan" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.retail = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.technology = [ " Australia" , " Austria" , " Brazil" , " Canada" , " China" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Taiwan" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.telecommunications = [ " Australia" , " Austria" , " Brazil" , " Canada" , " Czech Republic" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Ireland" , " Israel" , " Italy" , " Japan" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Panama" , " Paraguay" , " Peru" , " Philippines" , " Poland" , " Romania" , " Russia" , " Saudi Arabia" , " Singapore" , " South Africa" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];

		this.travel = [ " Argentina" , " Australia" , " Belgium" , " Brazil" , " Canada" , " Colombia" , " Denmark" , " Finland" , " France" , " Germany" , " Hong Kong" , " India" , " Indonesia" , " Ireland" , " Israel" , " Italy" , " Japan" , " Korea" , " Malaysia" , " Mexico" , " Netherlands" , " New Zealand" , " Norway" , " Philippines" , " Poland" , " Russia" , " Singapore" , " Singapore" , " Spain" , " Sweden" , " Switzerland" , " Turkey" , " United Arab Emirates" , " United Kingdom" , " United States Of America" , " Vietnam " ];


		this.healthcare=[ " Australia" , " Brazil" , " Canada" , " Germany" , " Italy" , " Japan" , " Mexico" , " New Zealand" , " Poland" , " Singapore" , " Spain" , " Switzerland" , " United Kingdom" , " United States Of America " ]

		this.testy = [ " Argentina" , " Australia" , " Austria" , " Belgium" , " Brazil" , " Canada" , " Czech Republic" , " Denmark " ];

		this.indusArray = [this.retail,this.entertainment,this.automotive,this.ecommerce,this.gaming,this.cpg,this.education,this.energyutil,this.financialservice,this.govtpoli, this.healthcare,this.orgassociations,this.proservices,this.technology,this.telecommunications,this.travel]

		this.butArray = new Array();
		this.countryDotArray = new Array();
		this.localTextArray = Hovermap.resources.iconArray;

		//this.localText2Array = Hovermap.resources.icon2Array;

		//this.localArray = Hovermap.resources.iconArray;

		//console.log("ArrayofObjectstest: " + this.localText2Array[1].xLoc)

		this.xLoop = 0;
		this.yLoop = 0;

		this.xLoopPlus = 0;

		this.xLoopTest = 0;

/*


	  	for (var i=0;i<46;i++){ 
	  		

	  		var temp = this.game.add.button(this.cords[i].xLoc, this.cords[i].yLoc, "icondot", this.actionOnClickCountry, this, 1, 0, 2);

	  		   temp.onInputOver.add(this.over, this);
               temp.onInputOut.add(this.out, this);
               temp.onInputUp.add(this.up, this);

            //This LAYS OUT THE BUTTONS ALONG THE BOTTOM
	  		//var temp = this.game.add.button( (this.xLoop * 70)-20, 520 + (this.yLoop * 50), this.butIconArray[i], this.actionOnClick, this, 1, 0, 2);
			
			temp.btnId = i;

            ///This is the array data coming from the JSON
			//temp.textData = this.localTextArray[i];
            //This is the array object data coming from the test coordinates above
			temp.textData = this.cords[i].btnText;

			temp.anchor.set(0.5);

			temp.alpha = 0;

			this.countryDotArray.push(temp);

		};

		this.testy2 = new Array();

        this.testy2 = this.indusArray[0];



		for (var n=0;n<46;n++){ 

			var nowBtn = this.countryDotArray[n];

			for (var q=0;q<this.testy2.length;q++){ 

			if (this.testy2[q] == nowBtn.textData){

			var tween1 = this.game.add.tween(nowBtn).to({alpha: 1}, 100, Phaser.Easing.Exponential.Out, true, 200);
			}
		}

	}

     
	this.icondotred = this.game.add.sprite(this.game.world.centerX -1000, this.game.world.centerY-1000, 'icondotred');
     this.icondotred.anchor.set(0.5);

    
*/


        for (var i=0;i<16;i++){ 
	  		

	  		//var temp = this.game.add.button(this.cords[i].xLoc, this.cords[i].yLoc, this.butIconArray[i], this.actionOnClick, this, 1, 0, 2);

			if (this.xLoop < 4){
				this.xLoop += 1; 
				
			}else{

				this.yLoop += 1;
				this.xLoop = 1 ;

			}


 //this.xLoopPlus = 5 ;

		

            //This LAYS OUT THE BUTTONS ALONG THE BOTTOM
	  		var temp = this.game.add.button( (this.xLoop * 160)-40, 150 + (this.yLoop * 90), this.industryIconArray[i], this.actionOnClick, this, 1, 0, 2);


	  		if (this.yLoop == 1 || this.yLoop == 3 ){

				 temp.x += 80;

				 //this.xLoopPlus + 2;

                 //this.xLoopTest = 1;
				 //this.xLoop += 1; 

			}
			
			temp.btnId = i;

			temp.scale.x = .8;
			temp.scale.y = .8;

            ///This is the array data coming from the JSON
			temp.textData = this.localTextArray[i];
   
		    temp.anchor.set(0.5);
			//temp.alpha = 0;

			var speedAlpha = 100 + i*100;
			//var tween1 = this.game.add.tween(temp).to({alpha: 1}, 100, Phaser.Easing.Exponential.Out, true, speedAlpha);
			this.butArray.push(temp);

		};


      this.redcirc = this.game.add.sprite(this.game.world.centerX - 1000, this.game.world.centerY-1000, 'redcirc')

     this.redcirc.anchor.set(0.5);

    // this.redcirc.scale.x = 2;
     //this.redcirc.scale.y = 2;


			//this.redcirc.x = .50;
			//this.redcirc.y = .50;
	},

	butOff: function (evt){



		//console.log("evt" + evt);
	    /*for (var i=0;i<this.countryDotArray.length;i++){ 

        var nowBut = this.countryDotArray[i];

        nowBut.alpha = 0; 

       //var nowParen = nowBut.parent;

		 //for (prop in nowParen){
          // console.log("prop : " + prop +" : "+ nowParen[prop]);
		//}

			//.textId.alpha = 0; 

		}*/
	},

	actionOnClick: function(evt){

	//this.butOn(evt);

	//for (prop in evt){
    // console.log("prop : " + prop +" : "+ evt[prop]);
    //}
	//console.log( " {btnText:'"+evt.textData+"' , xLoc:" + evt.x + " , yLoc:" + evt.y +" } ,")

    this.redcirc.x = evt.x;
	this.redcirc.y = evt.y;

	this.redcirc.scale.x = .50;
	this.redcirc.scale.y = .50;

    this.pfield_2.setText("");

	this.testy2 = new Array();

	//this.icondotred.x = 0;
	//this.icondotred.y = 0;

    this.testy2 = this.indusArray[evt.btnId];

	this.localArray = Hovermap.resources.iconArray;
	this.pfield_1.setText(this.localArray[evt.btnId]);

	this.pfield_2.setText(this.testy2);

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}