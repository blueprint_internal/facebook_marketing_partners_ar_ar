
function Flipcard(resources)
{
	Flipcard.resources = resources;
}
Flipcard.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 1000, Phaser.CANVAS, 'Flipcard', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{

		/*
            "bckg":"course/en/animations/flipcard/adspendbckg.png",
            "ico1text":"Campaign performance has plateaued",
            "ico2text":"You want to make use of external data",
            "ico3text":"You need help managing complexity",
            "ico4text":"You don't have the in-house expertise",
            "ico5text":"Recapture customers with dynamic retargeting",
            "ico6text":"Cross-platform measurement",
            "ico7text":"Integrating external data in innnovative ways",
            "ico8text":"Reaching new audiences",
            "card1":"course/en/animations/flipcard/card1.png",
            "card2":"course/en/animations/flipcard/card2.png",
            "card3":"course/en/animations/flipcard/card3.png",
            "card4":"course/en/animations/flipcard/card4.png"
		*/


        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 1000;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Flipcard.resources.bckg);
    	this.game.load.image('card1', Flipcard.resources.card1);
    	this.game.load.image('card2', Flipcard.resources.card2);
    	this.game.load.image('card3', Flipcard.resources.card3);
    	this.game.load.image('card4', Flipcard.resources.card4);
    	this.game.load.image('card1n', Flipcard.resources.card1n);
    	this.game.load.image('card2n', Flipcard.resources.card2n);
    	this.game.load.image('card3n', Flipcard.resources.card3n);
    	this.game.load.image('card4n', Flipcard.resources.card4n);



	},

	create: function(evt)
	{
     
    //Call the animation build function
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	buildAnimation: function()
	{


    //Background
    this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
    this.bckg.anchor.set(0.5);
    
    //STYLE
    var style = {font:"bold 50px freight-sans-pro", fill: "#FFFFFF", wordWrap: false,wordWrapWidth: 500, align: "left"};

    var styleFunnel = Flipcard.resources.card_style
        
        this.spacer = 100;
        this.topspace = 50
        
         this.space1 = this.spacer
         this.space2 = this.spacer+190
         this.space3 = this.spacer+380
         this.space4 = this.spacer+570

         this.cardscale = 1.5;


        this.card1 = this.game.add.sprite(this.spacer, this.topspace, 'card1');
    	this.card2 = this.game.add.sprite(this.spacer+190, this.topspace, 'card2');
		this.card3 = this.game.add.sprite(this.spacer+380, this.topspace, 'card3');
		this.card4 = this.game.add.sprite(this.spacer+570, this.topspace, 'card4');

		this.card1.scale.x = this.cardscale;
		this.card1.scale.y = this.cardscale;
		this.card2.scale.x = this.cardscale;
		this.card2.scale.y = this.cardscale;
		this.card3.scale.x = this.cardscale;
		this.card3.scale.y = this.cardscale;
		this.card4.scale.x = this.cardscale;
		this.card4.scale.y = this.cardscale;

		this.card1.x = this.spacer; 
		this.card2.x = (this.card2.width)  + this.spacer * 2; 
		this.card3.x = this.spacer;
		this.card4.x = (this.card2.width)  + this.spacer * 2;
		this.card3.y = this.card3.height + this.topspace * 2; 
		this.card4.y = this.card4.height + this.topspace * 2; 
		//this.card1.scale.y = this.cardscale;
	

		this.card1n = this.game.add.sprite(this.spacer, 50, 'card1n');
    	this.card2n = this.game.add.sprite(this.spacer+190, 50, 'card2n');
		this.card3n = this.game.add.sprite(this.spacer+380, 50, 'card3n');
		this.card4n = this.game.add.sprite(this.spacer+570, 50, 'card4n');


	    this.card1n.scale.x = this.cardscale;
		this.card1n.scale.y = this.cardscale;
		this.card2n.scale.x = this.cardscale;
		this.card2n.scale.y = this.cardscale;
		this.card3n.scale.x = this.cardscale;
		this.card3n.scale.y = this.cardscale;
		this.card4n.scale.x = this.cardscale;
		this.card4n.scale.y = this.cardscale;

		this.card1n.x = this.spacer; 
		this.card2n.x = (this.card2.width)  + this.spacer * 2; 
		this.card3n.x = this.spacer;
		this.card4n.x = (this.card2.width)  + this.spacer * 2;
		this.card3n.y = this.card3.height + this.topspace * 2; 
		this.card4n.y = this.card4.height + this.topspace * 2; 

		this.centerXCard = 170 /2;
		this.centerYCard = 200;



    //  Even if you release the mouse button outside of the game window
    //  the 'onUp' function will still be called.

    this.card1n.inputEnabled = true;
    this.card1n.events.onInputDown.add(this.actionOnClick, this);
    this.card1n.btnNum = 0;
    this.card1n.alpha = 0;

    this.card2n.inputEnabled = true;
    this.card2n.events.onInputDown.add(this.actionOnClick, this);
    this.card2n.btnNum = 0;
    this.card2n.alpha = 0;

    this.card3n.inputEnabled = true;
    this.card3n.events.onInputDown.add(this.actionOnClick, this);
    this.card3n.btnNum = 0;
    this.card3n.alpha = 0;

    this.card4n.inputEnabled = true;
    this.card4n.events.onInputDown.add(this.actionOnClick, this);
 	this.card4n.btnNum = 0;
 	this.card4n.alpha = 0;

/////////
	this.card1.btnNum = 1;
   // this.card1.inputEnabled = true;
   // this.card1.events.onInputDown.add(this.actionOnClickR, this);
    
    this.card2.btnNum = 2;
   // this.card2.inputEnabled = true;
    //this.card2.events.onInputDown.add(this.actionOnClickR, this);

    this.card3.btnNum = 3;
   // this.card3.inputEnabled = true;
    //this.card3.events.onInputDown.add(this.actionOnClickR, this);

 	this.card4.btnNum = 4;
  //  this.card4.inputEnabled = true;
    //this.card4.events.onInputDown.add(this.actionOnClickR, this);



   // bubble.events.onInputUp.add(onUp, this);



		//temp.addChild(this.pfield_N);
			//temp.addChild(this.game.make.sprite(0, 0, 'slab'));
		
		//
		//this.pfield_1 = this.game.add.text(this.space1 + this.centerXCard, 220, Flipcard.resources.ico1text, styleFunnel);


		this.pfield_1 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico1text, styleFunnel);
		this.pfield_1.anchor.set(0.5);
		this.card1.addChild(this.pfield_1);
		//
		this.pfield_2 = this.game.add.text( this.centerXCard, this.centerYCard, Flipcard.resources.ico2text, styleFunnel);
		this.pfield_2.anchor.set(0.5);
		this.card2.addChild(this.pfield_2);
		//
		this.pfield_3 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico3text, styleFunnel);
		this.pfield_3.anchor.set(0.5);
		this.card3.addChild(this.pfield_3);
		//
		this.pfield_4 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico4text, styleFunnel);
		this.pfield_4.anchor.set(0.5);
		this.card4.addChild(this.pfield_4);

		////////////

		this.pfield_5 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico5text, styleFunnel);
		this.pfield_5.anchor.set(0.5);
		this.card1n.addChild(this.pfield_5);
		//
		this.pfield_6 = this.game.add.text( this.centerXCard, this.centerYCard, Flipcard.resources.ico6text, styleFunnel);
		this.pfield_6.anchor.set(0.5);
		this.card2n.addChild(this.pfield_6);
		//
		this.pfield_7 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico7text, styleFunnel);
		this.pfield_7.anchor.set(0.5);
		this.card3n.addChild(this.pfield_7);
		//
		this.pfield_8 = this.game.add.text(this.centerXCard, this.centerYCard, Flipcard.resources.ico8text, styleFunnel);
		this.pfield_8.anchor.set(0.5);
		this.card4n.addChild(this.pfield_8);


	/*	
	this.tweencard1 = this.game.add.tween(this.card1n).to( { alpha: 0 }, 6000, Phaser.Easing.Linear.Out, true, 1000);

		this.tweencard2 = this.game.add.tween(this.card2n).to( { alpha: 0 }, 6000, Phaser.Easing.Linear.Out, true, 1000);

		this.tweencard3 = this.game.add.tween(this.card3n).to( { alpha: 0 }, 6000, Phaser.Easing.Linear.Out, true, 1000);

		this.tweencard4 = this.game.add.tween(this.card4n).to( { alpha: 0 }, 6000, Phaser.Easing.Linear.Out, true, 1000);
		*/


	/*
	this.tweencard1 = this.game.add.tween(this.card1).to( { x: 20 }, 1000, Phaser.Easing.Linear.Out, true, 1000);
	
	this.tweencard1Text = this.game.add.tween(this.pfield_1).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 2000);

	this.tweencard3 = this.game.add.tween(this.card3).to( { x: 20 }, 000, Phaser.Easing.Linear.Out, true, 4000);

	this.tweencard3Text = this.game.add.tween(this.pfield_2).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 5000);

	this.tweencard2 = this.game.add.tween(this.card2).to( { x: 20 }, 1000, Phaser.Easing.Linear.Out, true, 7000);

	this.tweencard2Text = this.game.add.tween(this.pfield_3).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 8000);
	*/

	},

	actionOnClick: function(evt){


	if(evt.btnNum > 0){

	evt.btnNum = 0;

	this.tweencard1 = this.game.add.tween(evt).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true, 100);
    }else{

	evt.btnNum = 1;

	this.tweencard1 = this.game.add.tween(evt).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.Out, true, 100);

    }

	  //evt.inputEnabled = false;

	},
/*
	actionOnClickR: function(evt){

		this.tweenTarg = this.card1;

			switch(evt.btnNum){
				case 1:
				this.tweenTarg =    this.card1;
				break;

				case 2:
				this.tweenTarg =    this.card2;
				break;
			}

		this.tweencard2 = this.game.add.tween(this.tweenTarg).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.Out, true, 100);

	},

	*/


	animate: function()
	{

	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{

	}

}